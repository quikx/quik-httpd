#pragma once

#pragma warning(disable:4028) // formal parameter different from declaration
#pragma warning(disable:4996) // deprecation
#pragma warning(disable:4267) // size_t to uint16_t warning
#pragma warning(disable:4244) // __int64 to int warning
#pragma warning(disable:4786) // identifier was truncated to '255' characters...

#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>

#include <stdexcept>
#include <sstream>
#include <memory>
#include <map>

#include <time.h>