#include "precomp.h"
#include "quik-http-server.h"
#include "fstring.h"
#include "parameters.h"

using namespace std;

QuikHttpServer::QuikHttpServer(QuikTerminal& quik) : quik(quik) {
  mg_mgr_init(&mgr, this);
}

QuikHttpServer::~QuikHttpServer() {
  mg_mgr_free(&mgr);
}

void QuikHttpServer::bind(const char* port) {
  const char* bind_error = "unknown error";
  mg_bind_opts bind_opts = { 0 };
  bind_opts.error_string = &bind_error;
  mg_connection* con = mg_bind_opt(&mgr, port, handler, bind_opts);
  if (con == NULL)
    throw runtime_error(fstring("Cannot bind server to port %s (%s)", port, bind_error));

  mg_set_protocol_http_websocket(con);
}

void QuikHttpServer::poll() {
  mg_mgr_poll(&mgr, 10);
}
