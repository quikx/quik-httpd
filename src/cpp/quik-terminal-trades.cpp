#include "precomp.h"
#include "quik-terminal.h"
#include "json.h"

using namespace std;

static inline int getNumberOfTrades(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "trades");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline void printTrade(ostream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "trades");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  os << "{";
  {
    lua_getfield(L, -1, "trade_num");
    os << printJsonMember("trade_num", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "order_num");
    os << ", " << printJsonMember("order_num", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "price");
    os << ", " << printJsonMember("price", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "price2");
    os << ", " << printJsonMember("price2", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "qty");
    os << ", " << printJsonMember("qty", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "value");
    os << ", " << printJsonMember("value", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "flags");
    os << ", " << printJsonMember("flags", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "clearing_comission");
    os << ", " << printJsonMember("clearing_comission", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "exchange_comission");
    os << ", " << printJsonMember("exchange_comission", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "tech_center_comission");
    os << ", " << printJsonMember("tech_center_comission", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "sec_code");
    os << ", " << printJsonMember("sec_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "class_code");
    os << ", " << printJsonMember("class_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "datetime");
    os << ", " << printJsonMember("datetime", datetime(L));
    lua_pop(L, 1);

    lua_getfield(L, -1, "period");
    os << ", " << printJsonMember("period", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "trans_id");
    os << ", " << printJsonMember("trans_id", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "side_qualifier");
    os << ", " << printJsonMember("side_qualifier", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);
  }
  os << "}";
  lua_pop(L, 1);
}

static int filterTradeByOrder(lua_State* L) {
  const char* searchingOrder = luaL_checkstring(L, lua_upvalueindex(1));
  const char* currentOrder = luaL_checkstring(L, 1);
  lua_pushboolean(L, strcmp(searchingOrder, currentOrder) == 0);
  return 1;
}

static int filterTradeByTransactionID(lua_State* L) {
  const char* searchingTxID = luaL_checkstring(L, lua_upvalueindex(1));
  const char* currentTxID = luaL_checkstring(L, 1);
  lua_pushboolean(L, strcmp(searchingTxID, currentTxID) == 0);
  return 1;
}

string QuikTerminal::getTrades(const parameters& params) {
  int trades = getNumberOfTrades(L);
  ostringstream os;
  os << "[";
  {
    string order = params.get("order");
    string txID = params.get("trans_id");
    if (!order.empty()) {
      // trade_indexes = SearchItems("trades", ..., filterTradeByOrder(number), ...)
      lua_getglobal(L, "SearchItems");
      lua_pushstring(L, "trades");
      lua_pushinteger(L, 0);
      lua_pushinteger(L, trades - 1);
      lua_pushstring(L, order.c_str());
      lua_pushcclosure(L, filterTradeByOrder, 1);
      lua_pushstring(L, "order_num");
      lua_call(L, 5, 1);
      if (!lua_isnil(L, -1)) {
        // look throw trade_indexes
        lua_pushnil(L);
        for (int i = 0; lua_next(L, -2) != 0; i++) {
          if (i > 0)
            os << ",";
          printTrade(os, L, (int)luaL_checkinteger(L, -1));
          lua_pop(L, 1);
        }
      }
      // pop trade_indexes
      lua_pop(L, 1);
    } else if (!txID.empty()) {
      // trade_indexes = SearchItems("trades", ..., filterTradeByTransactionID(number), ...)
      lua_getglobal(L, "SearchItems");
      lua_pushstring(L, "trades");
      lua_pushinteger(L, 0);
      lua_pushinteger(L, trades - 1);
      lua_pushstring(L, txID.c_str());
      lua_pushcclosure(L, filterTradeByTransactionID, 1);
      lua_pushstring(L, "trans_id");
      lua_call(L, 5, 1);
      if (!lua_isnil(L, -1)) {
        // look throw trade_indexes
        lua_pushnil(L);
        for (int i = 0; lua_next(L, -2) != 0; i++) {
          if (i > 0)
            os << ",";
          int trade = (int)luaL_checkinteger(L, -1);
          printTrade(os, L, trade);
          lua_pop(L, 1);
        }
      }
      // pop trade_indexes
      lua_pop(L, 1);
    }
    else
      for (int trade = 0; trade < trades; trade++) {
        if (trade > 0)
          os << ",";
        printTrade(os, L, trade);
      }
  }
  os << "]";
  return os.str();
}