#pragma once
#include <exception>
#include <string>

/**
 *
 */
class timeout : public std::exception {
  public:
    timeout() {}

    timeout(const std::string& message) : exception(message.c_str()) {}
};

class methodNotAllowed : public std::exception {};