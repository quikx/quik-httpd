#pragma once
#include <list>
#include <string>

/**
 *
 */
class split : public std::list<std::string> {
  public:
    split(const std::string& str, char splitter) {
      std::string::size_type start = 0, end = 0;
      for (; end < str.length(); end++) {
        if (str[end] == splitter) {
          if (end != start)
            push_back(str.substr(start, end - start));
          start = end + 1;
        }
      }
      if (end != start)
        push_back(str.substr(start, end - start));
    }

  private:
    split(const split&);

    split& operator=(const split&);
};


