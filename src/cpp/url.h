#pragma once

#include <mongoose/mongoose.h>
#include "parameters.h"

/**
 *
 */
class URLQueryParameters : public parameters {
  public:
    explicit URLQueryParameters(const mg_str& query) {
      using namespace std;
      string key, value;
      const char* token = query.p;
      const char* p = query.p;
      for (size_t i = 0; i < query.len; i++, p++) {
        switch (*p) {
          case '=':
            if (key.empty()) {
              key = string(token, p - token);
              token = p + 1;
              value = "";
            }
            break;
          case '&':
            if (key.empty())
              key = string(token, p - token);
            else value = string(token, p - token);

            if (!key.empty())
              insert(pair<string, string>(key, value));

            token = p + 1;
            key = value = "";
            break;
          default:
            break;
        }
      }
      if (token < p) {
        if (key.empty())
          key = string(token, p - token);
        else value = string(token, p - token);
        insert(pair<string, string>(key, value));
      }
    }

  private:
    URLQueryParameters(const URLQueryParameters&);

    URLQueryParameters& operator=(const URLQueryParameters&);
};
