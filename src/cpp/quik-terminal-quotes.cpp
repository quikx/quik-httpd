#include "precomp.h"
#include "cpp/quik-terminal.h"
#include "fstring.h"

using namespace std;

void QuikTerminal::subscribeQuotes(const parameters& params) {
  string cls = params.get("class");
  string sec = params.get("security");
  if (cls.empty())
    throw invalid_argument("Parameter 'class' is not specified");
  if (sec.empty())
    throw invalid_argument("Parameter 'security' is not specified");

  lua_getglobal(L, "Subscribe_Level_II_Quotes");
  lua_pushstring(L, cls.c_str());
  lua_pushstring(L, sec.c_str());
  lua_call(L, 2, 1);
  bool result = lua_toboolean(L, -1) != 0;
  lua_pop(L, 1);

  if (!result)
    throw domain_error(fstring("Subscribe to %s %s failed", cls.c_str(), sec.c_str()));
}