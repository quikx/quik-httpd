#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "json.h"

using namespace std;

static inline int getNumberOfStopOrders(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "stop_orders");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline string condition(const string& value) {
  if (value == "4")
    return "<=";
  if (value == "5")
    return ">=";
  return value;
}

static inline void printStopOrder(ostream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "stop_orders");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  os << "{";
  {
    lua_getfield(L, -1, "order_num");
    os << printJsonMember("order_num", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "stop_order_type");
    os << ", " << printJsonMember("stop_order_type", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "trans_id");
    os << ", " << printJsonMember("trans_id", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "sec_code");
    os << ", " << printJsonMember("sec_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "class_code");
    os << ", " << printJsonMember("class_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "flags");
    os << ", " << printJsonMember("flags", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "stopflags");
    os << ", " << printJsonMember("stopflags", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "firmid");
    os << ", " << printJsonMember("firmid", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "account");
    os << ", " << printJsonMember("account", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "qty");
    os << ", " << printJsonMember("qty", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "filled_qty");
    os << ", " << printJsonMember("filled_qty", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "condition");
    os << ", " << printJsonMember("condition", condition(luaL_checkstring(L, -1)));
    lua_pop(L, 1);

    lua_getfield(L, -1, "condition_price");
    os << ", " << printJsonMember("condition_price", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "price");
    os << ", " << printJsonMember("price", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "co_order_price");
    os << ", " << printJsonMember("co_order_price", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "balance");
    os << ", " << printJsonMember("balance", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "client_code");
    os << ", " << printJsonMember("client_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "order_date_time");
    os << ", " << printJsonMember("order_date_time", datetime(L));
    lua_pop(L, 1);

    lua_getfield(L, -1, "activation_date_time");
    os << ", " << printJsonMember("activation_date_time", datetime(L));
    lua_pop(L, 1);

    lua_getfield(L, -1, "withdraw_datetime");
    os << ", " << printJsonMember("withdraw_datetime", datetime(L));
    lua_pop(L, 1);

    lua_getfield(L, -1, "expiry");
    os << ", " << printJsonMember("expiry", luaL_checkstring(L, -1));
    lua_pop(L, 1);
  }
  os << "}";
  lua_pop(L, 1);
}

string QuikTerminal::getStopOrders() {
  ostringstream os;
  os << "[";
  int orders = getNumberOfStopOrders(L);
  for (int i = 0; i < orders; i++) {
    if (i > 0)
      os << ",";
    printStopOrder(os, L, i);
  }
  os << "]";
  return os.str();
}

static int filterStopOrderByNum(lua_State* L) {
  const char* searchingOrderNum = luaL_checkstring(L, lua_upvalueindex(1));
  const char* currentOrderNum = luaL_checkstring(L, 1);
  lua_pushboolean(L, strcmp(searchingOrderNum, currentOrderNum) == 0);
  return 1;
}

string QuikTerminal::getStopOrder(const string& number) {
  int ordersCount = getNumberOfStopOrders(L);

  // order_indexes = SearchItems("orders", ..., filterStopOrderByNum(number), ...)
  lua_getglobal(L, "SearchItems");
  lua_pushstring(L, "stop_orders");
  lua_pushinteger(L, 0);
  lua_pushinteger(L, ordersCount - 1);
  lua_pushstring(L, number.c_str());
  lua_pushcclosure(L, filterStopOrderByNum, 1);
  lua_pushstring(L, "order_num");
  lua_call(L, 5, 1);

  // If no order found return empty response (not exception)
  // because empty response is better then HTTP 400 or HTTP 404
  ostringstream os;
  if (!lua_isnil(L, -1)) {
    lua_rawgeti(L, -1, 1);
    printStopOrder(os, L, (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);
  }

  // pop order_indexes
  lua_pop(L, 1);
  return os.str();
}
