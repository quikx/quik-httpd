#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "json.h"

using namespace std;

static inline int getNumberOfDepoLimits(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "depo_limits");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline void printDepoLimit(ostringstream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "depo_limits");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  if (LUA_TTABLE != lua_type(L, -1)) {
    lua_pop(L, 1);
    throw runtime_error(fstring("Failed to get depo_limits at index %d", index));
  }
  os << "{";
  {
    lua_getfield(L, -1, "sec_code");
    os << printJsonMember("sec_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "trdaccid");
    os << ", " << printJsonMember("trdaccid", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "firmid");
    os << ", " << printJsonMember("firmid", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "client_code");
    os << ", " << printJsonMember("client_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "currentbal");
    os << ", " << printJsonMember("currentbal", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "currentlimit");
    os << ", " << printJsonMember("currentlimit", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "limit_kind");
    os << ", " << printJsonMember("limit_kind", string("T") + luaL_checkstring(L, -1));
    lua_pop(L, 1);
  }
  os << "}";
  lua_pop(L, 1);
}

string QuikTerminal::getDepoLimits() {
  ostringstream os;
  os << "[";
  int limits = getNumberOfDepoLimits(L);
  for (int i = 0; i < limits; i++) {
    if (i > 0)
      os << ",";
    printDepoLimit(os, L, i);
  }
  os << "]";
  return os.str();
}
