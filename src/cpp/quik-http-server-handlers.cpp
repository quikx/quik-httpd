#include "precomp.h"
#include "quik-http-server.h"
#include "fstring.h"
#include "url.h"
#include "exceptions.h"

using namespace std;

namespace TextPlain {
static inline void printOK(mg_connection* con, const string& response) {
  mg_send_head(
    con,
    response.empty() ? 204 : 200,
    response.length(),
    "Content-Type: text/plain; charset=windows-1251"
  );
  mg_send(con, response.c_str(), response.length());
}

static inline void printError(mg_connection* con, int code, const string& reason) {
  mg_send_head(
    con,
    code,
    reason.length(),
    "Content-Type: text/plain; charset=windows-1251"
    "\r\nConnection: close"
  );
  mg_send(con, reason.c_str(), reason.length());
  con->flags |= MG_F_SEND_AND_CLOSE;
}
}

namespace TextJson {
static inline void printOK(mg_connection* con, const string& response) {
  mg_send_head(
    con,
    response.empty() ? 204 : 200,
    response.length(),
    "Content-Type: application/json; charset=windows-1251"
  );
  mg_send(con, response.c_str(), response.length());
}
}

void QuikHttpServer::handler(mg_connection* con, int event, void* event_data) {
  try {
    if (event == MG_EV_HTTP_REQUEST) {
      // todo #7 log request
      http_message* msg = (http_message*) event_data;
      QuikHttpServer* self = static_cast<QuikHttpServer*>(con->mgr->user_data);
      // A. Order book is required most often
      if (0 == mg_vcmp(&msg->uri, "/order-book"))
        self->handleOrderBook(con, msg);

      // B. Ping is required often
      else if (0 == mg_vcmp(&msg->uri, "/ping"))
        self->handlePing(con, msg);

      // B. Money limits are required often
      else if (0 == mg_vcmp(&msg->uri, "/money-limits"))
        self->handleMoneyLimits(con, msg);

      // B. Orders are required often
      else if (0 == mg_vcmp(&msg->uri, "/order"))
        self->handleOrder(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/stop-order"))
        self->handleStopOrder(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/orders"))
        self->handleOrders(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/stop-orders"))
        self->handleStopOrders(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/trades"))
        self->handleTrades(con, msg);

      // C. Classes and trade accounts are required rarely
      else if (0 == mg_vcmp(&msg->uri, "/classes"))
        self->handleClasses(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/trade-accounts"))
        self->handleTradeAccounts(con, msg);

      // D. These services are almost not required
      else if (0 == mg_vcmp(&msg->uri, "/subscribe/quotes"))
        self->handleSubscribeQuotes(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/depo-limits"))
        self->handleDepoLimits(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/futures-client-holding"))
        self->handleFuturesClientHolding(con, msg);
      else if (0 == mg_vcmp(&msg->uri, "/firms"))
        self->handleFirms(con, msg);

      else mg_http_send_error(con, 404, "Not Found");
    }
  } catch (invalid_argument& e) {
    TextPlain::printError(con, 400, e.what());
  } catch (methodNotAllowed&) {
    TextPlain::printError(con, 405, "Method Not Allowed");
  } catch (timeout& e) {
    TextPlain::printError(con, 408, e.what());
  } catch (logic_error& e) {
    TextPlain::printError(con, 400, e.what());
  } catch (exception& e) {
    TextPlain::printError(con, 500, fstring("Server is broken down: %s", e.what()).c_str());
  } catch (...) {
    TextPlain::printError(con, 500, "Server is broken down with unknown error");
  }
}

inline void QuikHttpServer::handlePing(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextPlain::printOK(con, "QUIK HTTP daemon");
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleFirms(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextPlain::printOK(con, quik.getFirms());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleClasses(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextJson::printOK(con, quik.getClasses());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleTradeAccounts(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextJson::printOK(con, quik.getTradeAccounts());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleSubscribeQuotes(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "POST")) {
    URLQueryParameters params(msg->query_string);
    quik.subscribeQuotes(params);
    TextPlain::printOK(con, "");
  }
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleOrderBook(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET")) {
    URLQueryParameters params(msg->query_string);
    string cls = params["class"];
    string sec = params["security"];
    if (cls.empty())
      throw invalid_argument("Parameter 'class' is not specified");
    else if (sec.empty())
      throw invalid_argument("Parameter 'security' is not specified");

    orderbook book;
    quik.getOrderBook(book, cls, sec);
    TextJson::printOK(con, book.toJsonString());
  }
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleMoneyLimits(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextJson::printOK(con, quik.getMoneyLimits());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleDepoLimits(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextJson::printOK(con, quik.getDepoLimits());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleFuturesClientHolding(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextJson::printOK(con, quik.getFuturesClientHolding());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleOrder(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET")) {
    URLQueryParameters params(msg->query_string);
    TextJson::printOK(con, quik.getOrder(params));
  }
  else if (0 == mg_vcmp(&msg->method, "PUT")) {
    URLQueryParameters params(msg->query_string);
    string type = params.get("type");
    if (type == "market")
      TextJson::printOK(con, quik.putMarketOrder(params));
    else if (type == "limit")
      TextJson::printOK(con, quik.putLimitOrder(params));
    else throw invalid_argument(fstring("Unexpected order type: %s", type.c_str()));
  }
  else if (0 == mg_vcmp(&msg->method, "DELETE")) {
    URLQueryParameters params(msg->query_string);
    quik.killOrder(params);
    TextJson::printOK(con, "");
  }
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleStopOrder(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET")) {
    URLQueryParameters params(msg->query_string);
    TextJson::printOK(con, quik.getStopOrder(params.get("num")));
  }
  else if (0 == mg_vcmp(&msg->method, "PUT")) {
    URLQueryParameters params(msg->query_string);
    TextJson::printOK(con, quik.putStopOrder(params));
  }
  else if (0 == mg_vcmp(&msg->method, "DELETE")) {
    URLQueryParameters params(msg->query_string);
    quik.killStopOrder(params);
    TextJson::printOK(con, "");
  }
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleOrders(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET")) {
    URLQueryParameters params(msg->query_string);
    TextJson::printOK(con, quik.getOrders(params));
  }
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleStopOrders(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET"))
    TextJson::printOK(con, quik.getStopOrders());
  else throw methodNotAllowed();
}

inline void QuikHttpServer::handleTrades(mg_connection* con, const http_message* msg) {
  if (0 == mg_vcmp(&msg->method, "GET")) {
    URLQueryParameters params(msg->query_string);
    TextJson::printOK(con, quik.getTrades(params));
  }
  else throw methodNotAllowed();
}
