#include "precomp.h"
#include "quik-terminal.h"

using namespace std;

void QuikTerminal::printDbgString(const char* text, ...) {
  va_list ap;
    va_start(ap, text);
  lua_getglobal(L, "PrintDbgStr");
  string message = string("[quick_httpd] ") + text;
  lua_pushvfstring(L, message.c_str(), ap);
  lua_call(L, 1, 1);
  lua_pop(L, 1);
    va_end(ap);
}

void QuikTerminal::showErrorBox(const char *text, ...) {
  va_list ap;
    va_start(ap, text);
  string message = string("quick_httpd ") + text;
  lua_getglobal(L, "message");
  lua_pushvfstring(L, message.c_str(), ap);
  lua_pushinteger(L, 3);
  lua_call(L, 2, 1);
  lua_pop(L, 1);
    va_end(ap);
}
