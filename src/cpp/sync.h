#pragma once
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>

/**
 *
 */
class SyncObject {
  public:
    virtual void lock() = 0;

    virtual void unlock() = 0;

    virtual ~SyncObject() {}
};

/**
 *
 */
class CriticalSection : public SyncObject {
  private:
    CRITICAL_SECTION section;

  public:
    CriticalSection() {
      InitializeCriticalSection(&section);
    }

    void lock() {
      EnterCriticalSection(&section);
    }

    void unlock() {
      LeaveCriticalSection(&section);
    }

    ~CriticalSection() {
      DeleteCriticalSection(&section);
    }
};

/**
 *
 */
class Lock {
  private:
    SyncObject& sync;

  public:
    Lock(SyncObject& sync) : sync(sync) { sync.lock(); }

    ~Lock() { sync.unlock(); }
};