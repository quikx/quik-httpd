#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "split.h"
#include "json.h"

using namespace std;

static inline list<string> getClassesList(lua_State* L) {
  // returns OPTEXP,USDRUB,PSOPT,PSFUT,SPBFUT,
  lua_getglobal(L, "getClassesList");
  lua_call(L, 0, 1);
  string classesString = lua_tostring(L, -1);
  lua_pop(L, 1);
  return split(classesString, ',');
}

static inline void printClassInfo(ostream& os, lua_State* L, const string& clazz) {
  lua_getglobal(L, "getClassInfo");
  lua_pushstring(L, clazz.c_str());
  lua_call(L, 1, 1);
  {
    os << printJsonMember("code", clazz.c_str());

    lua_getfield(L, -1, "firmid");
    os << ", " + printJsonMember("firmid", luaL_checkstring(L, -1));
    lua_pop(L, 1);
  }
  lua_pop(L, 1);
}

static inline void printClassSecurities(ostream& os, lua_State* L, const string& clazz) {
  // returns RSH3,VBZ2,O4Z2,O2Z2,GMZ2,GZH3,GZZ2,EuZ2,EDZ2,SiZ2,RIZ2,
  lua_getglobal(L, "getClassSecurities");
  lua_pushstring(L, clazz.c_str());
  lua_call(L, 1, 1);
  string securitiesString = lua_tostring(L, -1);
  lua_pop(L, 1);

  os << "\"securities\": ";
  os << "[";
  list<string> securities = split(securitiesString, ',');
  list<string>::const_iterator it = securities.begin();
  for (; it != securities.end(); it++) {
    if (it != securities.begin())
      os << ", ";
    os << "\"" << *it << "\"";
  }
  os << "]";

}

string QuikTerminal::getClasses() {
  ostringstream os;
  os << "[";
  list<string> classes = getClassesList(L);
  list<string>::const_iterator it = classes.begin();
  for (; it != classes.end(); it++) {
    if (it != classes.begin())
      os << ", ";
    os << "{";
    {
      printClassInfo(os, L, *it);
      os << ", ";
      printClassSecurities(os, L, *it);
    }
    os << "}";
  }
  os << "]";
  return os.str();
}