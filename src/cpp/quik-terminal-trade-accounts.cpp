#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "split.h"
#include "json.h"

using namespace std;

static inline int getNumberOfTradeAccounts(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "trade_accounts");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline void printFirstField(ostream& os, lua_State* L, const char* field) {
  lua_getfield(L, -1, field);
  os << printJsonMember(field, luaL_checkstring(L, -1));
  lua_pop(L, 1);
}

static inline void printNextField(ostream& os, lua_State* L, const char* field) {
  lua_getfield(L, -1, field);
  os << ", " << printJsonMember(field, luaL_checkstring(L, -1));
  lua_pop(L, 1);
}

static inline void printTradeAccount(ostream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "trade_accounts");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  os << "{";
  {
    printFirstField(os, L, "trdaccid");
    printNextField(os, L, "trdacc_type");
    printNextField(os, L, "status");
    printNextField(os, L, "description");
    printNextField(os, L, "firmid");
    printNextField(os, L, "firmuse");
    printNextField(os, L, "fullcoveredsell");

    os << ", \"class_codes\": [";
    {
      lua_getfield(L, -1, "class_codes");
      list<string> classes = split(luaL_checkstring(L, -1), '|');
      list<string>::const_iterator itc = classes.begin();
      for (; itc != classes.end(); itc++) {
        if (itc != classes.begin())
          os << ", ";
        os << "\"" << *itc << "\"";
      }
      lua_pop(L, 1);
    }
    os << "]";
  }
  os << "}";
  lua_pop(L, 1);
}

string QuikTerminal::getTradeAccounts() {
  ostringstream os;
  os << "[";
  int accounts = getNumberOfTradeAccounts(L);
  for (int i = 0; i < accounts; i++) {
    if (i > 0)
      os << ",";
    printTradeAccount(os, L, i);
  }
  os << "]";
  return os.str();
}