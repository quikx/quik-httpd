#pragma once
#include <string>
#include <cstdarg>
#include <stdexcept>

#define FSTRING_BUFFER_SIZE 8192

// todo #27 move to string repo
class fstring : public std::string {
  public:
    fstring(const char* format, ...) {
      using namespace std;

      va_list ap;
        va_start(ap, format);
      char buff[FSTRING_BUFFER_SIZE] = { 0 };
      int size = _vsnprintf(buff, FSTRING_BUFFER_SIZE - 1, format, ap);
      if (size < 0)
        throw overflow_error(string("fstring buffer is not enough to write ") + format);
      assign(buff);
        va_end(ap);
    }

  private:
    fstring(const fstring&);

    fstring operator=(const fstring&);
};
