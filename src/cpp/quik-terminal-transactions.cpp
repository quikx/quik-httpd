#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "exceptions.h"

using namespace std;

/**
 * 2 seconds max to wait transaction and keep it in memory
 */
static const millis TRANSACTION_TIMEOUT = 2 * 1000;

enum TransactionStatus {
  TRANSACTION_HAS_SENT_TO_SERVER = 0,
  TRANSACTION_HAS_RECEIVED_BY_SERVER = 1,
  TRANSACTION_HAS_FAILED_WHEN_SENDING_TO_EXCHANGE = 2,
  TRANSACTION_HAS_EXECUTED = 3,
  TRANSACTION_HAS_FAILED_BY_EXCHANGE = 4,
  TRANSACTION_HAS_REJECTED_BY_SERVER_CHECKS = 5,
  TRANSACTION_HAS_REJECTED_BY_SERVER_LIMITS = 6,
  TRANSACTION_HAS_REJECTED_BY_EXCHANGE = 10,
  TRANSACTION_HAS_REJECTED_BY_DIGITAL_SIGNATURE = 11,
  TRANSACTION_HAS_FAILED_ON_TIMEOUT = 12,
  TRANSACTION_HAS_REJECTED_BECAUSE_CROSS_TRADE_RISK = 13,
  TRANSACTION_HAS_REJECTED_BECAUSE_BROKER_LIMITS = 14,
  TRANSACTION_HAS_ACCEPTED_AFTER_BROKER_CHECKS = 15,
  TRANSACTION_HAS_CANCELLED_BY_USER_ON_BROKER_CHECKS = 16,
};

void QuikTerminal::handleTransaction(const transaction& trans) {
  Lock lock(transactionsCriticalSection);

  millis now = clock.now();
  map<long, transaction>::iterator it = transactions.begin();
  for (; it != transactions.end();) {
    if ((now - it->second.timestamp) > TRANSACTION_TIMEOUT)
      it = transactions.erase(it);
    else it++;
  }

  transactions.insert(pair<long, transaction>(trans.id, trans));
}

transaction QuikTerminal::waitForTransactionComplete(long id) {
  Timer timer(clock, TRANSACTION_TIMEOUT);
  for (; !timer.isEnd(); timer.sleep(2)) {
    Lock lock(transactionsCriticalSection);
    map<long, transaction>::const_iterator it = transactions.find(id);
    if (it != transactions.end()) {
      const transaction& tx = it->second;
      switch (tx.status) {
        case TRANSACTION_HAS_SENT_TO_SERVER:
        case TRANSACTION_HAS_RECEIVED_BY_SERVER:
          // One of intermediate states; wait farther
          break;

        case TRANSACTION_HAS_EXECUTED:
        case TRANSACTION_HAS_ACCEPTED_AFTER_BROKER_CHECKS:
          // One of successful states
          return tx;

        case TRANSACTION_HAS_REJECTED_BY_SERVER_CHECKS:
        case TRANSACTION_HAS_REJECTED_BY_SERVER_LIMITS:
        case TRANSACTION_HAS_REJECTED_BY_EXCHANGE:
        case TRANSACTION_HAS_REJECTED_BY_DIGITAL_SIGNATURE:
        case TRANSACTION_HAS_REJECTED_BECAUSE_CROSS_TRADE_RISK:
        case TRANSACTION_HAS_REJECTED_BECAUSE_BROKER_LIMITS:
        case TRANSACTION_HAS_CANCELLED_BY_USER_ON_BROKER_CHECKS:
          // Invalid transaction params
          throw invalid_argument(fstring("Transaction has rejected: %s", tx.toString().c_str()));

        case TRANSACTION_HAS_FAILED_ON_TIMEOUT:
        case TRANSACTION_HAS_FAILED_BY_EXCHANGE:
        case TRANSACTION_HAS_FAILED_WHEN_SENDING_TO_EXCHANGE:
        default:
          // One of internal/unexpected errors
          throw domain_error(fstring("Transaction has failed: %s", tx.toString().c_str()));
      }
    }
  }
  throw timeout(fstring("Transaction has failed by timeout: id=%d", id));
}
