#pragma once
#include <lua/lua.hpp>
#include <string>
#include "clock.h"

/**
 *
 */
class transaction {
  public:
    lua_Integer id;

    lua_Integer status;

    std::string message;

    /**
     * creation time
     */
    millis timestamp;

    lua_Integer error_code;

    lua_Integer error_source;

    std::string order_num;

  public:
    transaction(lua_State* L, millis timestamp);

    std::string toString() const;
};

/**
 *
 */
class datetime : public std::string {
  public:
    datetime(lua_State* L);
};