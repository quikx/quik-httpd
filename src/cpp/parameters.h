#pragma once
#include <string>
#include <map>

class parameters : public std::map<std::string, std::string> {
  public:
    parameters() {}

    std::string get(const std::string& key) const {
      using namespace std;
      map<string, string>::const_iterator it = find(key);
      if (it != end())
        return it->second;
      return "";
    }

  private:
    parameters(const parameters&);

    parameters& operator=(const parameters&);
};
