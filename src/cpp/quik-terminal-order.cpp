#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "json.h"

using namespace std;

static inline void setTableField(lua_State* L, const char* key, const string& value) {
  if (!value.empty()) {
    lua_pushstring(L, value.c_str());
    lua_setfield(L, -2, key);
  }
}

static inline void setTableField(lua_State* L, const char* key, long value) {
  lua_pushstring(L, fstring("%d", value).c_str());
  lua_setfield(L, -2, key);
}

static inline string printPutOrderResponse(const transaction& tx) {
  ostringstream os;
  os << "{";
  {
    os << printJsonMember("trans_id", tx.id);
    os << ", " << printJsonMember("order_num", tx.order_num);
  }
  os << "}";
  return os.str();
}

string QuikTerminal::putLimitOrder(const parameters& params) {
  long txID = transactionCounter.next();

  lua_getglobal(L, "sendTransaction");
  lua_createtable(L, 10, 0);
  setTableField(L, "ACTION", "NEW_ORDER");
  setTableField(L, "TYPE", "L");
  setTableField(L, "TRANS_ID", txID);
  setTableField(L, "ACCOUNT", params.get("account"));
  setTableField(L, "CLIENT_CODE", params.get("client_code"));
  setTableField(L, "CLASSCODE", params.get("class"));
  setTableField(L, "SECCODE", params.get("security"));
  setTableField(L, "OPERATION", params.get("operation"));
  setTableField(L, "QUANTITY", params.get("quantity"));
  setTableField(L, "PRICE", params.get("price"));
  lua_call(L, 1, 1);

  string error = lua_tostring(L, -1);
  lua_pop(L, 1);
  if (!error.empty())
    throw invalid_argument(error);

  transaction tx = waitForTransactionComplete(txID);
  return printPutOrderResponse(tx);
}

string QuikTerminal::putMarketOrder(const parameters& params) {
  long txID = transactionCounter.next();

  lua_getglobal(L, "sendTransaction");
  lua_createtable(L, 10, 0);
  setTableField(L, "ACTION", "NEW_ORDER");
  setTableField(L, "TYPE", "M");
  setTableField(L, "TRANS_ID", txID);
  setTableField(L, "ACCOUNT", params.get("account"));
  setTableField(L, "CLIENT_CODE", params.get("client_code"));
  setTableField(L, "CLASSCODE", params.get("class"));
  setTableField(L, "SECCODE", params.get("security"));
  setTableField(L, "OPERATION", params.get("operation"));
  setTableField(L, "QUANTITY", params.get("quantity"));
  setTableField(L, "PRICE", "0");
  lua_call(L, 1, 1);

  string error = lua_tostring(L, -1);
  lua_pop(L, 1);
  if (!error.empty())
    throw domain_error(error);

  transaction tx = waitForTransactionComplete(txID);
  return printPutOrderResponse(tx);
}

string QuikTerminal::putStopOrder(const parameters& params) {
  string kind = params.get("stop_order_kind");
  if (kind.empty() || kind == "simple_stop_order")
    return putSimpleStopOrder(params);
  else if (kind == "with_linked_limit_order")
    return putStopOrderWithLinkedLimitOrder(params);
  else throw invalid_argument("Unexpected stop order kind " + kind);
}

string QuikTerminal::putSimpleStopOrder(const parameters& params) {
  long txID = transactionCounter.next();
  lua_getglobal(L, "sendTransaction");
  lua_createtable(L, 12, 0);
  setTableField(L, "ACTION", "NEW_STOP_ORDER");
  setTableField(L, "STOP_ORDER_KIND", "SIMPLE_STOP_ORDER");
  setTableField(L, "TRANS_ID", txID);
  setTableField(L, "ACCOUNT", params.get("account"));
  setTableField(L, "CLIENT_CODE", params.get("client_code"));
  setTableField(L, "CLASSCODE", params.get("class"));
  setTableField(L, "SECCODE", params.get("security"));
  setTableField(L, "OPERATION", params.get("operation"));
  setTableField(L, "QUANTITY", params.get("quantity"));
  setTableField(L, "STOPPRICE", params.get("stop_price"));
  setTableField(L, "PRICE", params.get("price"));
  setTableField(L, "EXPIRY_DATE", "TODAY"); // rise order until end of today
  lua_call(L, 1, 1);

  string error = lua_tostring(L, -1);
  lua_pop(L, 1);
  if (!error.empty())
    throw domain_error("QUIK: " + error);

  transaction tx = waitForTransactionComplete(txID);
  return printPutOrderResponse(tx);
}

string QuikTerminal::putStopOrderWithLinkedLimitOrder(const parameters& params) {
  long txID = transactionCounter.next();
  lua_getglobal(L, "sendTransaction");
  lua_createtable(L, 14, 0);
  setTableField(L, "ACTION", "NEW_STOP_ORDER");
  setTableField(L, "STOP_ORDER_KIND", "WITH_LINKED_LIMIT_ORDER");
  setTableField(L, "TRANS_ID", txID);
  setTableField(L, "ACCOUNT", params.get("account"));
  setTableField(L, "CLIENT_CODE", params.get("client_code"));
  setTableField(L, "CLASSCODE", params.get("class"));
  setTableField(L, "SECCODE", params.get("security"));
  setTableField(L, "OPERATION", params.get("operation"));
  setTableField(L, "QUANTITY", params.get("quantity"));
  setTableField(L, "STOPPRICE", params.get("stop_price"));
  setTableField(L, "PRICE", params.get("price"));
  setTableField(L, "LINKED_ORDER_PRICE", params.get("linked_order_price"));
  setTableField(L, "EXPIRY_DATE", "TODAY"); // rise order until end of today
  setTableField(L, "KILL_IF_LINKED_ORDER_PARTLY_FILLED", "NO");
  lua_call(L, 1, 1);

  string error = lua_tostring(L, -1);
  lua_pop(L, 1);
  if (!error.empty())
    throw domain_error("QUIK: " + error);

  transaction tx = waitForTransactionComplete(txID);
  return printPutOrderResponse(tx);
}

void QuikTerminal::killOrder(const parameters& params) {
  long txID = transactionCounter.next();

  lua_getglobal(L, "sendTransaction");
  lua_createtable(L, 5, 0);
  setTableField(L, "ACTION", "KILL_ORDER");
  setTableField(L, "TRANS_ID", txID);
  setTableField(L, "CLASSCODE", params.get("class"));
  setTableField(L, "SECCODE", params.get("security"));
  setTableField(L, "ORDER_KEY", params.get("order_key"));
  lua_call(L, 1, 1);
  const char* error = lua_tostring(L, -1);
  lua_pop(L, 1);
  if (error != NULL && *error != 0)
    throw domain_error(error);

  waitForTransactionComplete(txID);
}

void QuikTerminal::killStopOrder(const parameters& params) {
  long txID = transactionCounter.next();

  lua_getglobal(L, "sendTransaction");
  lua_createtable(L, 5, 0);
  setTableField(L, "ACTION", "KILL_STOP_ORDER");
  setTableField(L, "TRANS_ID", txID);
  setTableField(L, "CLASSCODE", params.get("class"));
  setTableField(L, "SECCODE", params.get("security"));
  setTableField(L, "STOP_ORDER_KEY", params.get("stop_order_key"));
  lua_call(L, 1, 1);
  string error = lua_tostring(L, -1);
  lua_pop(L, 1);
  if (!error.empty())
    throw domain_error(error);

  waitForTransactionComplete(txID);
}
