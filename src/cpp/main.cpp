#include "precomp.h"
#include "quik-http-server.h"

static bool volatile started = false;

static Clock* volatile pClock = NULL;

static QuikTerminal* volatile pQuik = NULL;

static int start(lua_State* L) {
  Clock clock;
  QuikTerminal quik(L, clock);

  // todo: #25 critical section
  if (started) {
    quik.showErrorBox("QuikHttpServer already started");
    return 1;
  }

  const char* port = luaL_checkstring(L, -1);
  if (!port || !*port) {
    quik.showErrorBox("Cannot start server: port is not specified");
    return 1;
  }

  pClock = &clock;
  pQuik = &quik;
  try {
    QuikHttpServer httpServer(quik);
    httpServer.bind(port);
    for (started = true; started;)
      httpServer.poll();
  } catch (std::exception& e) {
    started = false;
    quik.showErrorBox("QuikHttpServer is broken down: %s", e.what());
    pQuik = NULL;
    pClock = NULL;
    return 1;
  } catch (...) {
    started = false;
    quik.showErrorBox("QuikHttpServer is broken down with unknown error");
    pQuik = NULL;
    pClock = NULL;
    return 1;
  }

  started = false;
  return 0;
}

static int stop(lua_State*) {
  started = false;
  pQuik = NULL;
  pClock = NULL;
  return 0;
}

static int handleTransaction(lua_State* L) {
  if (pClock != NULL) {
    transaction trans(L, pClock->now());
    lua_pop(L, 1);
    if (pQuik != NULL)
      pQuik->handleTransaction(trans);
  }
  else
    lua_pop(L, 1);
  return 0;
}

static const struct luaL_Reg QuikHttpServer[] = {
  { "start", start },
  { "stop", stop },
  { "handleTransaction", handleTransaction },
  { NULL, NULL }
};

static void registerLibForLua(lua_State* L, const char* libname, const luaL_Reg* lib) {
  lua_getglobal(L, libname);
  if (lua_isnil(L, -1)) {
    lua_pop(L, 1);
    const luaL_Reg* libEnd = lib;
    while (libEnd->name != NULL)
      libEnd++;
    lua_createtable(L, 0, (libEnd - lib) / sizeof(luaL_Reg));
  }
  luaL_setfuncs(L, lib, 0);
}

extern "C" LUALIB_API int luaopen_QuikHttpServer(lua_State* L) {
  registerLibForLua(L, "QuikHttpServer", QuikHttpServer);
  return 1;
}
