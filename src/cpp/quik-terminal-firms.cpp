#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "json.h"

using namespace std;

static inline int getNumberOfFirms(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "firms");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline void printFirstField(ostream& os, lua_State* L, const char* field) {
  lua_getfield(L, -1, field);
  os << printJsonMember(field, luaL_checkstring(L, -1));
  lua_pop(L, 1);
}

static inline void printNextField(ostream& os, lua_State* L, const char* field) {
  lua_getfield(L, -1, field);
  os << ", " << printJsonMember(field, luaL_checkstring(L, -1));
  lua_pop(L, 1);
}

static inline void printFirm(ostream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "firms");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  os << "{";
  printFirstField(os, L, "firmid");
  printNextField(os, L, "firm_name");
  printNextField(os, L, "status");
  printNextField(os, L, "exchange");
  os << "}";
  lua_pop(L, 1);
}

string QuikTerminal::getFirms() {
  ostringstream os;
  os << "[";
  int firms = getNumberOfFirms(L);
  for (int i = 0; i < firms; i++) {
    if (i > 0)
      os << ",";
    printFirm(os, L, i);
  }
  os << "]";
  return os.str();
}