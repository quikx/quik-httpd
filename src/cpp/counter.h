#pragma once
#include "clock.h"
#include "sync.h"

struct Counter {
  public:
    virtual long next() = 0;

    virtual ~Counter() {}
};

/**
 * Counter, based on current count of millis starting from current day
 */
class DayMillisCounter : public Counter {
  private:
    Clock& clock;

    const short scale;

    long counter;

    CriticalSection counterCriticalSection;

  public:
    DayMillisCounter(Clock& clock, short scale)
      : clock(clock), scale(scale), counter(0) {}

    long next() {
      return time() * scale + count();
    }

  private:
    long time() {
      return static_cast<long>(clock.now() - clock.today());
    }

    long count() {
      Lock lock(counterCriticalSection);
      counter++;
      if (counter >= scale)
        counter = 1;
      return counter;
    }
};