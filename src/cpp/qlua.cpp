#include "precomp.h"
#include "qlua.h"
#include "fstring.h"

using namespace std;

transaction::transaction(lua_State* L, millis timestamp) {
  // Here on top of L we have a table with transaction params
  lua_getfield(L, -1, "trans_id");
  this->id = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "status");
  this->status = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "result_msg");
  this->message = luaL_checkstring(L, -1);
  lua_pop(L, 1);

  this->timestamp = timestamp;

  lua_getfield(L, -1, "error_code");
  this->error_code = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "error_source");
  this->error_source = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "order_num");
  if (!lua_isnil(L, -1))
    this->order_num = luaL_checkstring(L, -1);
  lua_pop(L, 1);
}

std::string transaction::toString() const {
  return fstring(
    "{id=%d, status=%d, message=%s, timestamp=%d, error_code=%d, error_source=%d}",
    id, status, message.c_str(), timestamp, error_code, error_source
  );
}

datetime::datetime(lua_State* L) {
  // Here on top of L we have a table with date_time
  lua_getfield(L, -1, "year");
  lua_Integer year = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "month");
  lua_Integer month = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "day");
  lua_Integer day = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "hour");
  lua_Integer hour = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "min");
  lua_Integer min = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "sec");
  lua_Integer sec = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  lua_getfield(L, -1, "ms");
  lua_Integer ms = luaL_checkinteger(L, -1);
  lua_pop(L, 1);

  assign(
    fstring(
      "%d-%02d-%02dT%02d:%02d:%02d.%03d",
      year, month, day, hour, min, sec, ms
    ));
}
