#include "precomp.h"
#include "cpp/quik-terminal.h"

using namespace std;

order::order(const std::string& price, const std::string& quantity)
  : price(price), quantity(quantity) {}

string order::toJsonString() const {
  return string()
    + "{"
    + "\"price\": " + "\"" + price + "\""
    + ", "
    + "\"quantity\": " + "\"" + quantity + "\""
    + "}";
}

////////////////////////////////////////////////////////////////////////////////

string orderbook::toJsonString() const {
  return string()
    + "{"
    + "\"asks\": " + ordersToJson(asks)
    + ", "
    + "\"bids\": " + ordersToJson(bids)
    + "}";
}

string orderbook::ordersToJson(const list<order>& orders) const {
  ostringstream os;
  os << "[";
  if (!orders.empty()) {
    list<order>::const_iterator it = orders.begin();
    os << static_cast<const order>(*it).toJsonString();
    for (it++; it != orders.end(); it++) {
      os << ", " << static_cast<const order>(*it).toJsonString();
    }
  }
  os << "]";
  return os.str();
}

////////////////////////////////////////////////////////////////////////////////

void QuikTerminal::getOrderBook(orderbook& book, const string& cls, const string& sec) {
  book.asks.clear();
  book.bids.clear();

  lua_getglobal(L, "getQuoteLevel2");
  lua_pushstring(L, cls.c_str());
  lua_pushstring(L, sec.c_str());
  lua_call(L, 2, 1);
  //lua_assert(LUA_TTABLE == lua_type(L, 1));
  {
    lua_getfield(L, -1, "bid_count");
    int bid_count = (int)luaL_checkinteger(L, -1);
    lua_pop(L, 1);

    lua_getfield(L, -1, "offer_count");
    int offer_count = (int)luaL_checkinteger(L, -1);
    lua_pop(L, 1);

    int i;
    lua_getfield(L, -1, "bid");
    for (i = 0; i < bid_count; i++) {
      lua_rawgeti(L, -1, 1 + i);
      {
        lua_getfield(L, -1, "price");
        string price = luaL_checkstring(L, -1);
        lua_pop(L, 1);

        lua_getfield(L, -1, "quantity");
        string quantity = luaL_checkstring(L, -1);
        lua_pop(L, 1);

        book.bids.push_back(order(price, quantity));
      }
      lua_pop(L, 1);
    }
    lua_pop(L, 1);

    lua_getfield(L, -1, "offer");
    for (i = 0; i < offer_count; i++) {
      lua_rawgeti(L, -1, 1 + i);
      {
        lua_getfield(L, -1, "price");
        string price = luaL_checkstring(L, -1);
        lua_pop(L, 1);

        lua_getfield(L, -1, "quantity");
        string quantity = luaL_checkstring(L, -1);
        lua_pop(L, 1);

        book.asks.push_back(order(price, quantity));
      }
      lua_pop(L, 1);
    }
    lua_pop(L, 1);
  }
  lua_pop(L, 1);
}
