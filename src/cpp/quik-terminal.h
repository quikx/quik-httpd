#pragma once
#include "qlua.h"
#include "parameters.h"
#include <string>
#include <list>
#include <map>
#include "sync.h"
#include "clock.h"
#include "counter.h"

class order {
  public:
    const std::string price;
    const std::string quantity;

  public:
    order(const std::string& price, const std::string& quantity);

  public:
    std::string toJsonString() const;
};

class orderbook {
  public:
    std::list<order> asks;
    std::list<order> bids;

  public:
    std::string toJsonString() const;

  private:
    std::string ordersToJson(const std::list<order>& orders) const;
};

/**
 * QUIK terminal on-lua implementation
 */
class QuikTerminal {
  private:
    lua_State* L;

    Clock& clock;

    std::map<long, transaction> transactions;

    CriticalSection transactionsCriticalSection;

    DayMillisCounter transactionCounter;

  public:
    QuikTerminal(lua_State* L, Clock& clock)
      : L(L), clock(clock), transactionCounter(clock, 24) {}

  public:
    void printDbgString(const char* text, ...);

    void showErrorBox(const char* text, ...);

    void subscribeQuotes(const parameters& params);

    void getOrderBook(orderbook& book, const std::string& cls, const std::string& sec);

    std::string getFirms();

    std::string getClasses();

    std::string getTradeAccounts();

    std::string getMoneyLimits();

    std::string getDepoLimits();

    std::string getFuturesClientHolding();

    std::string getOrders(const parameters& params);

    std::string getStopOrders();

    std::string getOrder(const parameters& params);

    std::string getStopOrder(const std::string& number);

    std::string putLimitOrder(const parameters& params);

    std::string putMarketOrder(const parameters& params);

    void killOrder(const parameters& params);

    std::string putStopOrder(const parameters& params);

    std::string putSimpleStopOrder(const parameters& params);

    std::string putStopOrderWithLinkedLimitOrder(const parameters& params);

    std::string getTrades(const parameters& params);

    void killStopOrder(const parameters& params);

    void handleTransaction(const transaction& trans);

  private:
    transaction waitForTransactionComplete(long id);
};