#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "json.h"

using namespace std;

static inline int getNumberOfOrders(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "orders");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline void printOrder(ostream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "orders");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  os << "{";
  {
    lua_getfield(L, -1, "order_num");
    os << printJsonMember("order_num", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "trans_id");
    os << ", " << printJsonMember("trans_id", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "sec_code");
    os << ", " << printJsonMember("sec_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "class_code");
    os << ", " << printJsonMember("class_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "flags");
    os << ", " << printJsonMember("flags", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "ext_order_status");
    os << ", " << printJsonMember("ext_order_status", (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "firmid");
    os << ", " << printJsonMember("firmid", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "account");
    os << ", " << printJsonMember("account", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "qty");
    os << ", " << printJsonMember("qty", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "price");
    os << ", " << printJsonMember("price", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "awg_price");
    os << ", " << printJsonMember("awg_price", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "price_currency");
    os << ", " << printJsonMember("price_currency", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "balance");
    os << ", " << printJsonMember("balance", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "value");
    os << ", " << printJsonMember("value", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "client_code");
    os << ", " << printJsonMember("client_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "activation_time");
    os << ", " << printJsonMember("activation_time", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "datetime");
    os << ", " << printJsonMember("datetime", datetime(L));
    lua_pop(L, 1);

    lua_getfield(L, -1, "withdraw_datetime");
    os << ", " << printJsonMember("withdraw_datetime", datetime(L));
    lua_pop(L, 1);

    lua_getfield(L, -1, "expiry");
    os << ", " << printJsonMember("expiry", luaL_checkstring(L, -1));
    lua_pop(L, 1);
  }
  os << "}";
  lua_pop(L, 1);
}

static int filterOrderByString(lua_State* L) {
  const char* searchingString = luaL_checkstring(L, lua_upvalueindex(1));
  const char* currentString = luaL_checkstring(L, 1);
  lua_pushboolean(L, strcmp(searchingString, currentString) == 0);
  return 1;
}

////////////////////////////////////////////////////////////////////////////////

static inline string getOrdersByTransID(lua_State* L, const string& tx) {
  int ordersCount = getNumberOfOrders(L);

  // order_indexes = SearchItems("orders", ..., filterOrderByString(tx), ...)
  lua_getglobal(L, "SearchItems");
  lua_pushstring(L, "orders");
  lua_pushinteger(L, 0);
  lua_pushinteger(L, ordersCount - 1);
  lua_pushstring(L, tx.c_str());
  lua_pushcclosure(L, filterOrderByString, 1);
  lua_pushstring(L, "trans_id");
  lua_call(L, 5, 1);

  // If no order found return empty array (not exception)
  // because empty response is better then HTTP 400 or HTTP 404
  ostringstream os;
  os << "[";
  if (!lua_isnil(L, -1)) {
    // look throw order_indexes
    lua_pushnil(L);
    for (int i = 0; lua_next(L, -2) != 0; i++) {
      if (i > 0)
        os << ",";
      printOrder(os, L, (int)luaL_checkinteger(L, -1));
      lua_pop(L, 1);
    }
  }
  os << "]";

  // pop order_indexes
  lua_pop(L, 1);
  return os.str();
}

string QuikTerminal::getOrders(const parameters& params) {
  string tx = params.get("trans_id");
  if (!tx.empty())
    return getOrdersByTransID(L, tx);

  ostringstream os;
  os << "[";
  int orders = getNumberOfOrders(L);
  for (int i = 0; i < orders; i++) {
    if (i > 0)
      os << ",";
    printOrder(os, L, i);
  }
  os << "]";
  return os.str();
}

////////////////////////////////////////////////////////////////////////////////

static inline string getOrderByNum(lua_State* L, const string& number) {
  int ordersCount = getNumberOfOrders(L);

  // order_indexes = SearchItems("orders", ..., filterOrderByString(number), ...)
  lua_getglobal(L, "SearchItems");
  lua_pushstring(L, "orders");
  lua_pushinteger(L, 0);
  lua_pushinteger(L, ordersCount - 1);
  lua_pushstring(L, number.c_str());
  lua_pushcclosure(L, filterOrderByString, 1);
  lua_pushstring(L, "order_num");
  lua_call(L, 5, 1);

  // If no order found return empty response (not exception)
  // because empty response is better then HTTP 400 or HTTP 404
  ostringstream os;
  if (!lua_isnil(L, -1)) {
    lua_rawgeti(L, -1, 1);
    printOrder(os, L, (int)luaL_checkinteger(L, -1));
    lua_pop(L, 1);
  }

  // pop order_indexes
  lua_pop(L, 1);
  return os.str();
}

string QuikTerminal::getOrder(const parameters& params) {
  string number = params.get("num");
  if (!number.empty())
    return getOrderByNum(L, number);

  throw invalid_argument("No valid argument specified for get-order");
}