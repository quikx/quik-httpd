#pragma once
#include "quik-terminal.h"
#include <mongoose/mongoose.h>

/**
 *
 */
class QuikHttpServer {
  private:
    QuikTerminal &quik;
    mg_mgr mgr;

  public:
    QuikHttpServer(QuikTerminal &quik);

    void bind(const char *port);

    void poll();

    ~QuikHttpServer();

  private:
    static void handler(mg_connection *con, int event, void *event_data);

    void handlePing(mg_connection* con, const http_message* msg);

    void handleFirms(mg_connection* con, const http_message* msg);

    void handleClasses(mg_connection* con, const http_message* msg);

    void handleTradeAccounts(mg_connection* con, const http_message* msg);

    void handleSubscribeQuotes(mg_connection* con, const http_message* msg);

    void handleOrderBook(mg_connection* con, const http_message* msg);

    void handleMoneyLimits(mg_connection* con, const http_message* msg);

    void handleDepoLimits(mg_connection* con, const http_message* msg);

    void handleFuturesClientHolding(mg_connection* con, const http_message* msg);

    void handleOrder(mg_connection* con, const http_message* msg);

    void handleStopOrder(mg_connection* con, const http_message* msg);

    void handleOrders(mg_connection* con, const http_message* msg);

    void handleStopOrders(mg_connection* con, const http_message* msg);

    void handleTrades(mg_connection* con, const http_message* msg);
};
