#include "precomp.h"
#include "quik-terminal.h"
#include "fstring.h"
#include "json.h"

using namespace std;

static inline int getNumberOfFuturesClientHoldings(lua_State* L) {
  lua_getglobal(L, "getNumberOf");
  lua_pushstring(L, "futures_client_holding");
  lua_call(L, 1, 1);
  int number = (int)luaL_checkinteger(L, -1);
  lua_pop(L, 1);
  return number;
}

static inline void printFuturesClientHolding(ostringstream& os, lua_State* L, int index) {
  lua_getglobal(L, "getItem");
  lua_pushstring(L, "futures_client_holding");
  lua_pushinteger(L, index);
  lua_call(L, 2, 1);
  if (LUA_TTABLE != lua_type(L, -1)) {
    lua_pop(L, 1);
    throw runtime_error(fstring("Failed to get futures_client_holding at index %d", index));
  }
  os << "{";
  {
    lua_getfield(L, -1, "firmid");
    os << printJsonMember("firmid", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "trdaccid");
    os << ", " << printJsonMember("trdaccid", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "sec_code");
    os << ", " << printJsonMember("sec_code", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "todaybuy");
    os << ", " << printJsonMember("todaybuy", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "todaysell");
    os << ", " << printJsonMember("todaysell", luaL_checkstring(L, -1));
    lua_pop(L, 1);

    lua_getfield(L, -1, "totalnet");
    os << ", " << printJsonMember("totalnet", luaL_checkstring(L, -1));
    lua_pop(L, 1);
  }
  os << "}";
  lua_pop(L, 1);
}

string QuikTerminal::getFuturesClientHolding() {
  ostringstream os;
  os << "[";
  int holdings = getNumberOfFuturesClientHoldings(L);
  for (int i = 0; i < holdings; i++) {
    if (i > 0)
      os << ",";
    printFuturesClientHolding(os, L, i);
  }
  os << "]";
  return os.str();
}
