#pragma once

#include <string>
#include <sstream>
#include "fstring.h"

inline std::string escapeJsonString(const char* str) {
  std::ostringstream escaped;
  for (int i = 0;; i++) {
    char c = str[i];
    switch (c) {
      default:
        escaped << c;
        break;
      case 0x8:
        escaped << "\\b";
        break;
      case 0x9:
        escaped << "\\t";
        break;
      case 0xA:
        escaped << "\\n";
        break;
      case 0xC:
        escaped << "\\f";
        break;
      case 0xD:
        escaped << "\\r";
        break;
      case '"':
        escaped << "\\\"";
        break;
      case '\\':
        escaped << "\\\\";
        break;
      case 0:
        return escaped.str();
    }
  }
}

inline std::string printJsonMember(const char* member, const char* value) {
  return fstring("\"%s\": \"%s\"", member, escapeJsonString(value).c_str());
}

inline std::string printJsonMember(const char* member, const std::string& value) {
  return fstring("\"%s\": \"%s\"", member, escapeJsonString(value.c_str()).c_str());
}

inline std::string printJsonMember(const char* member, int value) {
  return fstring("\"%s\": %d", member, value);
}