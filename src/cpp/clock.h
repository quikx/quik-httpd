#pragma once
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>

typedef time_t millis;

class Clock {
  public:
    millis now() {
      SYSTEMTIME sysTime = { 0 };
      GetSystemTime(&sysTime);
      return toMillis(sysTime);
    }

    millis today() {
      SYSTEMTIME sysTime = { 0 };
      GetSystemTime(&sysTime);
      sysTime.wHour = 0;
      sysTime.wMinute = 0;
      sysTime.wSecond = 0;
      sysTime.wMilliseconds = 0;
      return toMillis(sysTime);
    }

  private:
    millis toMillis(const SYSTEMTIME& sysTime) {
      FILETIME fileTime = { 0 };
      SystemTimeToFileTime(&sysTime, &fileTime);

      ULARGE_INTEGER largeTime = { 0 };
      largeTime.LowPart = fileTime.dwLowDateTime;
      largeTime.HighPart = fileTime.dwHighDateTime;

      // fileTime contains hundred of nanos
      return largeTime.QuadPart / (1000000 / 100);
    }
};

/**
 *
 */
class Timer {
  private:
    Clock& clock;

    const millis end;

  public:
    Timer(Clock& clock, int timeout)
      : clock(clock), end(clock.now() + timeout) {}

    void sleep(int millis) {
      Sleep(static_cast<DWORD>(millis));
    }

    bool isEnd() const {
      return clock.now() >= end;
    }
};
