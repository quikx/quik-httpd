package.cpath = "${LIB_QUIK_HTTPD}.dll"
Server = require "QuikHttpServer";

-- Entry point to qlua script execution process under QUIK terminal
-- QUIK executes main() in separate thread
function main()
  Server.start("8080")
end

-- Callback for Stop script execution event
function OnStop()
  Server.stop();
end;

-- Callback for terminal exiting event.
function OnClose()
  Server.stop();
end;

function OnTransReply(trans)
  Server.handleTransaction(trans)
end;