# -----------------------------------------------------------------------
# Platform.
# -----------------------------------------------------------------------
# Determine the platform
if(WIN32)
  set(PLATFORM_NAME "win")
else()
  message(FATAL_ERROR "Unsupported platform.")
endif()

# Determine platform bits.
# Determine platform bits.
if(CMAKE_SIZEOF_VOID_P EQUAL 4)
  set(PLATFORM_BITNESS "32")
elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(PLATFORM_BITNESS "64")
else()
  message("Unsupported bitness")
endif()

# Set PLATFORM.
set(PLATFORM "${PLATFORM_NAME}${PLATFORM_BITNESS}")

message("Platform is ${PLATFORM}")

# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
# Runtime environment.
# -----------------------------------------------------------------------

if(MSVC)
  set(CPP_NAME "msvc")
endif()

if(MSVC60)
  set(CRT "vc6")
elseif(MSVC70 OR MSVC71)
  set(CRT "vc7")
elseif(MSVC80)
  set(CRT "vc8")
elseif(MSVC90)
  set(CRT "vc9")
elseif(MSVC10)
  set(CRT "vc10")
elseif(MSVC11)
  set(CRT "vc11")
elseif(MSVC12)
  set(CRT "vc12")
elseif(MSVC13)
  set(CRT "vc13")
elseif(MSVC14)
  set(CRT "vc14")
elseif(MSVC15)
  set(CRT "vc15")
else()
  message(FATAL_ERROR "Unsupported CRT.")
endif()

message("CRT is ${CRT}")
