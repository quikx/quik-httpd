if (MSVC)

  # -MTd = static debug library
  # -Od = disable optimization
  # -Gm = minimal rebuild
  # -ZI = debug&continue
  # -RTCc = Smaller Type Check
  # -FR = Enable browse information
  set(CMAKE_CXX_FLAGS_DEBUG "/MTd /Od /Gm /ZI /RTCc /FR")
  set(CMAKE_C_FLAGS_DEBUG ${CMAKE_CXX_FLAGS_DEBUG})

  # -MT = static release library
  # -O2 = Maximize Speed
  set(CMAKE_CXX_FLAGS_RELEASE "/MT /O2")
  set(CMAKE_C_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE})
endif ()

add_definitions(-D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE)
